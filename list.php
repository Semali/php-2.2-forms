<?php
error_reporting(E_ALL);
include 'functions.php';
?>

<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	</head>
	<body>
		<p>Список загруженных тестов:</p>
		<?php if (count(showTestNames()) == 0) { ?>
			<p>Нет загруженных тестов.</p>
		<?php } else { ?>
			<?php foreach (showTestNames() as $test) { ?>
				<p><?php echo $test; ?></p>
			<?php } ?>
		<?php } ?>
		<p>Перейти к загрузке тестов: <a href="./admin.php">жмак</a>.</p>
		<p>Перейти к выбору теста: <a href="./test.php">жмак</a>.</p>
	</body>
</html>