<?php
function getExt($fileName) {
    return substr($fileName, strrpos($fileName, '.') + 1);
}

function createDir() {
    $uploadDir = 'download-tests';
    // Проверка и создание директории
    if (!file_exists($uploadDir)) {
        mkdir($uploadDir);
    }
}

function uploadFile($inputFile, $allowedExt = ['json']) {
    $uploadDir = 'download-tests';
    if (isset($_FILES[$inputFile])) {
        $ext = getExt($_FILES[$inputFile]['name']);

        if (!in_array($ext, $allowedExt)) {
            return false;
        }

        createDir();

        // Номер теста
        $number = count(array_diff(scandir($uploadDir), array('..', '.'))) + 1;

        $sourceFile = $_FILES[$inputFile]['tmp_name'];

        $fileName = "$number.$ext";
        $destinationFile = realpath(__DIR__ . "/$uploadDir") . '/' . $fileName;
        if(move_uploaded_file($sourceFile, $destinationFile)) {
            return "$uploadDir/$fileName";
        } else {
            return false;
        }
    }
}

function showTestNames() {
    createDir();
    // Массив загруженных тестов в папке
    return array_diff(scandir('download-tests'), array('..', '.'));
}

function getName($n) {
    // Вырезаем имена тестов
    return substr($n, 0, strrpos($n, '.'));
}

function decodeTest($n) {
    // Расшифровка JSON-файла
    $json = file_get_contents('./download-tests' . '/' . $n . '.json');
    return json_decode($json, true);
}

function getTest() {
    if (isset($_GET['name'])) {
        // Массив имен тестов
        $names = array_map('getName', showTestNames());

        // Проверка на наличие теста
        if (!in_array($_GET['name'], $names)) {
            return false;
        }

        return decodeTest($_GET['name']);
    }
}

function getAnswers($array) {
    // $test = decodeTest($_POST["test-name"]);
    // $answer = array_search($n, $key in $_POST);

    if ($array["answer"] === $_POST[$array["id"]]) {
        return true;
    } else {
        return false;
    }
    
}