<?php
include 'functions.php';

$test = uploadFile('test');

if (!$test) {
    die('При загрузке файла произошла ошибка!');
}
?>

<p>Тест успешно загружен.</p>
<p>Загрузить еще тест: <a href="./admin.php">жмак</a>.</p>
<p>Перейти к списку тестов: <a href="./list.php">жмак</a>.</p>
<p>Перейти к выбору теста: <a href="./test.php">жмак</a>.</p>